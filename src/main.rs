mod external_memory;

use std::{
    ffi::{CStr, CString},
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
};

use {
    ash::{
        extensions::{
            ext::DebugUtils,
            khr::{Surface, Swapchain, Win32Surface},
        },
        util::Align,
        version::{DeviceV1_0, EntryV1_0, InstanceV1_0},
        vk::{self, Handle},
        Device, Entry, Instance,
    },
    openvr::{ApplicationType, Eye},
};

use external_memory::ExternalMemoryWin32;

fn main() {
    let running = Arc::new(AtomicBool::new(true));
    let r = running.clone();

    ctrlc::set_handler(move || {
        r.store(false, Ordering::SeqCst);
    })
    .unwrap();

    unsafe {
        let openvr = openvr::init(ApplicationType::Scene).unwrap();
        let system = openvr.system().unwrap();
        let compositor = openvr.compositor().unwrap();
        let _render_models = openvr.render_models().unwrap();
        let _chaperone = openvr.chaperone().unwrap();

        let context = VulkanContext::new();
        let present_context = PresentContext::new(&context.entry, &system, &compositor);

        let device_memory_properties = context
            .instance
            .get_physical_device_memory_properties(context.physical_device);

        let pool_create_info = vk::CommandPoolCreateInfo::builder()
            .flags(vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER)
            .queue_family_index(context.queue_family_index);
        let command_pool = context
            .device
            .create_command_pool(&pool_create_info, None)
            .unwrap();

        let command_buffer_allocate_info = vk::CommandBufferAllocateInfo::builder()
            .command_buffer_count(1)
            .command_pool(command_pool)
            .level(vk::CommandBufferLevel::PRIMARY);
        let command_buffer = context
            .device
            .allocate_command_buffers(&command_buffer_allocate_info)
            .unwrap()[0];

        // Texture data
        let (tex_width, tex_height) = system.recommended_render_target_size();
        let mut texture_bytes = Vec::new();
        for _ in 0..tex_height {
            for x in 0..tex_width {
                if (x % 32) < 16 {
                    texture_bytes.push(255u8);
                    texture_bytes.push(255u8);
                    texture_bytes.push(255u8);
                    texture_bytes.push(255u8);
                } else {
                    texture_bytes.push(255u8);
                    texture_bytes.push(0u8);
                    texture_bytes.push(0u8);
                    texture_bytes.push(255u8);
                }
            }
        }

        let image_buffer_info = vk::BufferCreateInfo {
            size: (std::mem::size_of::<u8>() * texture_bytes.len()) as u64,
            usage: vk::BufferUsageFlags::TRANSFER_SRC,
            sharing_mode: vk::SharingMode::EXCLUSIVE,
            ..Default::default()
        };
        let image_buffer = context
            .device
            .create_buffer(&image_buffer_info, None)
            .unwrap();
        let image_buffer_memory_req = context.device.get_buffer_memory_requirements(image_buffer);
        let image_buffer_memory_index = find_memorytype_index(
            &image_buffer_memory_req,
            &device_memory_properties,
            vk::MemoryPropertyFlags::HOST_VISIBLE | vk::MemoryPropertyFlags::HOST_COHERENT,
        )
        .expect("Unable to find suitable memorytype for the vertex buffer.");

        let image_buffer_allocate_info = vk::MemoryAllocateInfo {
            allocation_size: image_buffer_memory_req.size,
            memory_type_index: image_buffer_memory_index,
            ..Default::default()
        };
        let image_buffer_memory = context
            .device
            .allocate_memory(&image_buffer_allocate_info, None)
            .unwrap();
        let image_ptr = context
            .device
            .map_memory(
                image_buffer_memory,
                0,
                image_buffer_memory_req.size,
                vk::MemoryMapFlags::empty(),
            )
            .unwrap();
        let mut image_slice = Align::new(
            image_ptr,
            std::mem::align_of::<u8>() as u64,
            image_buffer_memory_req.size,
        );
        image_slice.copy_from_slice(&texture_bytes);
        context.device.unmap_memory(image_buffer_memory);
        context
            .device
            .bind_buffer_memory(image_buffer, image_buffer_memory, 0)
            .unwrap();

        let texture_create_info = vk::ImageCreateInfo {
            image_type: vk::ImageType::TYPE_2D,
            format: vk::Format::R8G8B8A8_SRGB,
            extent: vk::Extent3D {
                width: tex_width,
                height: tex_height,
                depth: 1,
            },
            mip_levels: 1,
            array_layers: 1,
            samples: vk::SampleCountFlags::TYPE_1,
            tiling: vk::ImageTiling::OPTIMAL,
            usage: vk::ImageUsageFlags::TRANSFER_DST
                | vk::ImageUsageFlags::TRANSFER_SRC
                | vk::ImageUsageFlags::SAMPLED,
            sharing_mode: vk::SharingMode::EXCLUSIVE,
            ..Default::default()
        };
        let texture_image = context
            .device
            .create_image(&texture_create_info, None)
            .unwrap();
        let texture_memory_req = context.device.get_image_memory_requirements(texture_image);
        let texture_memory_index = find_memorytype_index(
            &texture_memory_req,
            &device_memory_properties,
            vk::MemoryPropertyFlags::DEVICE_LOCAL,
        )
        .expect("Unable to find suitable memory index for depth image.");

        let mut export_alloc = vk::ExportMemoryAllocateInfo::builder().handle_types(
            vk::ExternalMemoryHandleTypeFlags::EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32,
        );
        let texture_allocate_info = vk::MemoryAllocateInfo::builder()
            .push_next(&mut export_alloc)
            .allocation_size(texture_memory_req.size)
            .memory_type_index(texture_memory_index);

        let texture_memory = context
            .device
            .allocate_memory(&texture_allocate_info, None)
            .unwrap();
        context
            .device
            .bind_image_memory(texture_image, texture_memory, 0)
            .expect("Unable to bind depth image memory");
        let get_info = vk::MemoryGetWin32HandleInfoKHR::builder()
            .memory(texture_memory)
            .handle_type(
                vk::ExternalMemoryHandleTypeFlags::EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32,
            );
        let texture_memory_handle = context
            .external_memory
            .get_memory_win32_handle(&get_info)
            .unwrap();

        // Copy data to image
        context
            .device
            .reset_command_buffer(
                command_buffer,
                vk::CommandBufferResetFlags::RELEASE_RESOURCES,
            )
            .unwrap();

        let command_buffer_begin_info = vk::CommandBufferBeginInfo::builder()
            .flags(vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT);
        context
            .device
            .begin_command_buffer(command_buffer, &command_buffer_begin_info)
            .unwrap();

        let texture_barrier = vk::ImageMemoryBarrier {
            dst_access_mask: vk::AccessFlags::TRANSFER_WRITE,
            new_layout: vk::ImageLayout::TRANSFER_DST_OPTIMAL,
            image: texture_image,
            subresource_range: vk::ImageSubresourceRange {
                aspect_mask: vk::ImageAspectFlags::COLOR,
                level_count: 1,
                layer_count: 1,
                ..Default::default()
            },
            ..Default::default()
        };
        context.device.cmd_pipeline_barrier(
            command_buffer,
            vk::PipelineStageFlags::BOTTOM_OF_PIPE,
            vk::PipelineStageFlags::TRANSFER,
            vk::DependencyFlags::empty(),
            &[],
            &[],
            &[texture_barrier],
        );
        let buffer_copy_regions = vk::BufferImageCopy::builder()
            .image_subresource(
                vk::ImageSubresourceLayers::builder()
                    .aspect_mask(vk::ImageAspectFlags::COLOR)
                    .layer_count(1)
                    .build(),
            )
            .image_extent(vk::Extent3D {
                width: tex_width,
                height: tex_height,
                depth: 1,
            });

        context.device.cmd_copy_buffer_to_image(
            command_buffer,
            image_buffer,
            texture_image,
            vk::ImageLayout::TRANSFER_DST_OPTIMAL,
            &[buffer_copy_regions.build()],
        );

        // This texture needs to be ready for transfer src for openvr
        let texture_barrier_end = vk::ImageMemoryBarrier {
            src_access_mask: vk::AccessFlags::TRANSFER_WRITE,
            dst_access_mask: vk::AccessFlags::TRANSFER_READ,
            old_layout: vk::ImageLayout::TRANSFER_DST_OPTIMAL,
            new_layout: vk::ImageLayout::TRANSFER_SRC_OPTIMAL,
            image: texture_image,
            subresource_range: vk::ImageSubresourceRange {
                aspect_mask: vk::ImageAspectFlags::COLOR,
                level_count: 1,
                layer_count: 1,
                ..Default::default()
            },
            ..Default::default()
        };
        context.device.cmd_pipeline_barrier(
            command_buffer,
            vk::PipelineStageFlags::TRANSFER,
            vk::PipelineStageFlags::TRANSFER,
            vk::DependencyFlags::empty(),
            &[],
            &[],
            &[texture_barrier_end],
        );

        context.device.end_command_buffer(command_buffer).unwrap();

        let submit_fence = context
            .device
            .create_fence(&vk::FenceCreateInfo::default(), None)
            .expect("Create fence failed.");

        let command_buffers = vec![command_buffer];

        let submit_info = vk::SubmitInfo::builder().command_buffers(&command_buffers);

        context
            .device
            .queue_submit(context.queue, &[submit_info.build()], submit_fence)
            .unwrap();

        context
            .device
            .wait_for_fences(&[submit_fence], true, std::u64::MAX)
            .unwrap();

        // Cleanup after copy is complete
        context.device.destroy_buffer(image_buffer, None);
        context.device.free_memory(image_buffer_memory, None);
        context.device.destroy_fence(submit_fence, None);

        // Create the mapped image on the present side
        let texture_create_info = vk::ImageCreateInfo {
            image_type: vk::ImageType::TYPE_2D,
            format: vk::Format::R8G8B8A8_SRGB,
            extent: vk::Extent3D {
                width: tex_width,
                height: tex_height,
                depth: 1,
            },
            mip_levels: 1,
            array_layers: 1,
            samples: vk::SampleCountFlags::TYPE_1,
            tiling: vk::ImageTiling::OPTIMAL,
            usage: vk::ImageUsageFlags::TRANSFER_DST
                | vk::ImageUsageFlags::TRANSFER_SRC
                | vk::ImageUsageFlags::SAMPLED,
            sharing_mode: vk::SharingMode::EXCLUSIVE,
            ..Default::default()
        };
        let present_image = present_context
            .device
            .create_image(&texture_create_info, None)
            .unwrap();

        let mut import_alloc = vk::ImportMemoryWin32HandleInfoKHR::builder()
            .handle_type(
                vk::ExternalMemoryHandleTypeFlags::EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32,
            )
            .handle(texture_memory_handle);
        let present_allocate_info = vk::MemoryAllocateInfo::builder()
            .push_next(&mut import_alloc)
            .allocation_size(texture_memory_req.size)
            .memory_type_index(texture_memory_index);

        let present_memory = present_context
            .device
            .allocate_memory(&present_allocate_info, None)
            .unwrap();
        present_context
            .device
            .bind_image_memory(present_image, present_memory, 0)
            .expect("Unable to bind depth image memory");

        compositor.set_tracking_space(openvr::TrackingUniverseOrigin::Standing);

        // Present loop
        while running.load(Ordering::Relaxed) {
            compositor.wait_get_poses().unwrap();

            let vulkan_texture = openvr::compositor::texture::vulkan::Texture {
                image: present_image.as_raw(),
                device: present_context.device.handle().as_raw() as *mut openvr::VkDevice_T,
                physical_device: present_context.physical_device.as_raw()
                    as *mut openvr::VkPhysicalDevice_T,
                instance: present_context.instance.handle().as_raw() as *mut openvr::VkInstance_T,
                queue: present_context.queue.as_raw() as *mut openvr::VkQueue_T,
                queue_family_index: present_context.queue_family_index,
                width: tex_width,
                height: tex_height,
                format: vk::Format::R8G8B8A8_SRGB.as_raw() as u32,
                sample_count: 1,
            };

            let texture = openvr::compositor::texture::Texture {
                handle: openvr::compositor::texture::Handle::Vulkan(vulkan_texture),
                color_space: openvr::compositor::texture::ColorSpace::Auto,
            };

            compositor.submit(Eye::Left, &texture, None, None).unwrap();
            compositor.submit(Eye::Right, &texture, None, None).unwrap();
        }

        context.device.device_wait_idle().unwrap();
        present_context.device.device_wait_idle().unwrap();

        // Cleanup
        present_context.device.destroy_image(present_image, None);
        present_context.device.free_memory(present_memory, None);

        context.device.destroy_image(texture_image, None);
        context.device.free_memory(texture_memory, None);
        context.device.destroy_command_pool(command_pool, None);

        present_context.destroy();
        context.destroy();
    }
}

struct VulkanContext {
    entry: Entry,
    instance: Instance,
    debug_loader: DebugUtils,
    messenger: vk::DebugUtilsMessengerEXT,

    physical_device: vk::PhysicalDevice,
    device: Device,
    queue_family_index: u32,
    queue: vk::Queue,

    external_memory: ExternalMemoryWin32,
}

impl VulkanContext {
    pub unsafe fn new() -> Self {
        println!("creating vulkan context");

        let entry = Entry::new().unwrap();

        let layers = vec![CString::new("VK_LAYER_KHRONOS_validation").unwrap()];
        let extensions = vec![DebugUtils::name().to_owned()];

        let instance = create_instance(&entry, "vkvrtest", layers, extensions);

        let (debug_loader, messenger) = create_debug_utils_messenger(&entry, &instance);

        // Pick a physical device
        let physical_device = instance.enumerate_physical_devices().unwrap()[0];

        let queue_family_index = instance
            .get_physical_device_queue_family_properties(physical_device)
            .iter()
            .enumerate()
            .filter(|(_, info)| info.queue_flags.contains(vk::QueueFlags::GRAPHICS))
            .next()
            .unwrap()
            .0 as u32;

        let features = instance.get_physical_device_features(physical_device);
        let priorities = [1.0];

        let queue_info = [vk::DeviceQueueCreateInfo::builder()
            .queue_family_index(queue_family_index)
            .queue_priorities(&priorities)
            .build()];

        let extension_names = [ExternalMemoryWin32::name().as_ptr()];
        let device_create_info = vk::DeviceCreateInfo::builder()
            .queue_create_infos(&queue_info)
            .enabled_extension_names(&extension_names)
            .enabled_features(&features);

        let device = instance
            .create_device(physical_device, &device_create_info, None)
            .unwrap();

        let queue = device.get_device_queue(queue_family_index, 0);

        let external_memory = ExternalMemoryWin32::new(&instance, &device);

        Self {
            entry,
            instance,
            debug_loader,
            messenger,

            physical_device,
            device,
            queue_family_index,
            queue,

            external_memory,
        }
    }

    pub unsafe fn destroy(&self) {
        self.device.destroy_device(None);
        self.debug_loader
            .destroy_debug_utils_messenger(self.messenger, None);
        self.instance.destroy_instance(None);
    }
}

struct PresentContext {
    instance: Instance,

    physical_device: vk::PhysicalDevice,
    device: Device,
    queue_family_index: u32,
    queue: vk::Queue,
}

impl PresentContext {
    pub unsafe fn new(
        entry: &Entry,
        system: &openvr::System,
        compositor: &openvr::Compositor,
    ) -> Self {
        println!("creating present context");

        let layers = vec![];
        let extensions = compositor.vulkan_instance_extensions_required();

        let instance = create_instance(entry, "vkvrtest", layers, extensions);

        // Pick a physical device
        let vr_physical_device =
            system.vulkan_output_device(instance.handle().as_raw() as *mut openvr::VkInstance_T);
        let physical_device = if let Some(physical_device) = vr_physical_device {
            vk::PhysicalDevice::from_raw(physical_device as u64)
        } else {
            println!("Falling back to picking first physical device");
            let physical_device = instance.enumerate_physical_devices().unwrap();
            physical_device[0]
        };

        let queue_family_index = instance
            .get_physical_device_queue_family_properties(physical_device)
            .iter()
            .enumerate()
            .filter(|(_, info)| info.queue_flags.contains(vk::QueueFlags::GRAPHICS))
            .next()
            .unwrap()
            .0 as u32;

        let features = instance.get_physical_device_features(physical_device);
        let priorities = [1.0];

        let queue_info = [vk::DeviceQueueCreateInfo::builder()
            .queue_family_index(queue_family_index)
            .queue_priorities(&priorities)
            .build()];

        let mut device_extension_names_raw = vec![
            Swapchain::name().as_ptr(),
            ExternalMemoryWin32::name().as_ptr(),
        ];

        let openvr_extensions = compositor.vulkan_device_extensions_required(
            physical_device.as_raw() as *mut openvr::VkPhysicalDevice_T,
        );

        for extension in &openvr_extensions {
            device_extension_names_raw.push(extension.as_ptr());
        }

        let device_create_info = vk::DeviceCreateInfo::builder()
            .queue_create_infos(&queue_info)
            .enabled_extension_names(&device_extension_names_raw)
            .enabled_features(&features);

        let device = instance
            .create_device(physical_device, &device_create_info, None)
            .unwrap();

        let queue = device.get_device_queue(queue_family_index, 0);

        Self {
            instance,
            physical_device,
            device,
            queue_family_index,
            queue,
        }
    }

    pub unsafe fn destroy(&self) {
        self.device.destroy_device(None);
        self.instance.destroy_instance(None);
    }
}

pub unsafe fn create_instance(
    entry: &Entry,
    application_name: &str,
    layers: Vec<CString>,
    extensions: Vec<CString>,
) -> Instance {
    let app_name = CString::new(application_name).unwrap();
    let engine_name = CString::new("vroge-gpu-vk").unwrap();

    println!("extensions: {:?}", extensions);

    let app_info = vk::ApplicationInfo::builder()
        .application_name(&app_name)
        .application_version(0)
        .engine_name(&engine_name)
        .engine_version(0)
        .api_version(vk::make_version(1, 1, 0));

    let layers_names_raw: Vec<_> = layers.iter().map(|raw_name| raw_name.as_ptr()).collect();

    let mut extension_names_raw = vec![Surface::name().as_ptr(), Win32Surface::name().as_ptr()];
    for extension in &extensions {
        extension_names_raw.push(extension.as_ptr());
    }

    let instance_info = vk::InstanceCreateInfo::builder()
        .application_info(&app_info)
        .enabled_layer_names(&layers_names_raw)
        .enabled_extension_names(&extension_names_raw);

    entry.create_instance(&instance_info, None).unwrap()
}

pub unsafe fn create_debug_utils_messenger(
    entry: &Entry,
    instance: &Instance,
) -> (DebugUtils, vk::DebugUtilsMessengerEXT) {
    let debug_utils_loader = DebugUtils::new(entry, instance);

    let debug_utils_messenger_info = vk::DebugUtilsMessengerCreateInfoEXT::builder()
        .message_severity(
            vk::DebugUtilsMessageSeverityFlagsEXT::ERROR
                | vk::DebugUtilsMessageSeverityFlagsEXT::WARNING,
        )
        .message_type(
            vk::DebugUtilsMessageTypeFlagsEXT::GENERAL
                | vk::DebugUtilsMessageTypeFlagsEXT::PERFORMANCE
                | vk::DebugUtilsMessageTypeFlagsEXT::VALIDATION,
        )
        .pfn_user_callback(Some(vulkan_messenger_callback))
        .build();

    let debug_utils_messenger = debug_utils_loader
        .create_debug_utils_messenger(&debug_utils_messenger_info, None)
        .unwrap();

    (debug_utils_loader, debug_utils_messenger)
}

unsafe extern "system" fn vulkan_messenger_callback(
    _message_severity: vk::DebugUtilsMessageSeverityFlagsEXT,
    _message_type: vk::DebugUtilsMessageTypeFlagsEXT,
    p_callback_data: *const vk::DebugUtilsMessengerCallbackDataEXT,
    _p_user_data: *mut std::ffi::c_void,
) -> vk::Bool32 {
    let message = CStr::from_ptr((*p_callback_data).p_message).to_string_lossy();
    println!("{}\n", message);
    vk::FALSE
}

pub fn find_memorytype_index(
    memory_req: &vk::MemoryRequirements,
    memory_prop: &vk::PhysicalDeviceMemoryProperties,
    flags: vk::MemoryPropertyFlags,
) -> Option<u32> {
    // Try to find an exactly matching memory flag
    let best_suitable_index =
        find_memorytype_index_f(memory_req, memory_prop, flags, |property_flags, flags| {
            property_flags == flags
        });
    if best_suitable_index.is_some() {
        return best_suitable_index;
    }
    // Otherwise find a memory flag that works
    find_memorytype_index_f(memory_req, memory_prop, flags, |property_flags, flags| {
        property_flags & flags == flags
    })
}

pub fn find_memorytype_index_f<F: Fn(vk::MemoryPropertyFlags, vk::MemoryPropertyFlags) -> bool>(
    memory_req: &vk::MemoryRequirements,
    memory_prop: &vk::PhysicalDeviceMemoryProperties,
    flags: vk::MemoryPropertyFlags,
    f: F,
) -> Option<u32> {
    let mut memory_type_bits = memory_req.memory_type_bits;
    for (index, ref memory_type) in memory_prop.memory_types.iter().enumerate() {
        if memory_type_bits & 1 == 1 && f(memory_type.property_flags, flags) {
            return Some(index as u32);
        }
        memory_type_bits >>= 1;
    }
    None
}
