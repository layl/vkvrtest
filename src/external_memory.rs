use crate::vk;
use ash::prelude::*;
use ash::version::{DeviceV1_0, InstanceV1_0};
use std::ffi::CStr;
use std::mem;

#[derive(Clone)]
pub struct ExternalMemoryWin32 {
    handle: vk::Device,
    external_memory_win32_fn: vk::KhrExternalMemoryWin32Fn,
}

impl ExternalMemoryWin32 {
    pub fn new<I: InstanceV1_0, D: DeviceV1_0>(instance: &I, device: &D) -> Self {
        let external_memory_win32_fn = vk::KhrExternalMemoryWin32Fn::load(|name| unsafe {
            mem::transmute(instance.get_device_proc_addr(device.handle(), name.as_ptr()))
        });
        Self {
            handle: device.handle(),
            external_memory_win32_fn,
        }
    }

    pub fn name() -> &'static CStr {
        vk::KhrExternalMemoryWin32Fn::name()
    }

    pub unsafe fn get_memory_win32_handle(
        &self,
        create_info: &vk::MemoryGetWin32HandleInfoKHR,
    ) -> VkResult<vk::HANDLE> {
        let mut handle = std::ptr::null_mut();
        let err_code = self.external_memory_win32_fn.get_memory_win32_handle_khr(
            self.handle,
            create_info,
            &mut handle,
        );
        match err_code {
            vk::Result::SUCCESS => Ok(handle),
            _ => Err(err_code),
        }
    }

    /*pub unsafe fn get_memory_win32_handle_properties(
        &self,
        handle_type: vk::ExternalMemoryHandleTypeFlags,
        handle: vk::HANDLE,
    ) -> VkResult<vk::MemoryWin32HandlePropertiesKHR> {
        let mut memory_handle_properties = mem::zeroed();
        let err_code = self.external_memory_win32_fn.get_memory_win32_handle_properties_khr(
            self.handle,
            handle_type,
            handle,
            &mut memory_handle_properties,
        );
        match err_code {
            vk::Result::SUCCESS => Ok(memory_handle_properties),
            _ => Err(err_code),
        }
    }

    pub fn fp(&self) -> &vk::KhrExternalMemoryWin32Fn {
        &self.external_memory_win32_fn
    }

    pub fn device(&self) -> vk::Device {
        self.handle
    }*/
}
